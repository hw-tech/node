var express = require('express');
var router = express.Router();
var connection = require('../conn');
var sha1 = require('sha1');

const mysql = require('mysql');

// Find all [ใช้ร่วมกัน]
router.get('/all/:flag', function(req, res, next) {
  let query;

  if(req.params.flag === 'N'){
    query = "SELECT * FROM `user_login` ul " +
            "INNER JOIN `user` u ON u.user_id = ul.user_id ";
  } else {
    query = "SELECT * FROM `user_login` ul " +
            "INNER JOIN `admin` a ON a.admin_id = ul.user_id ";
  }

  let options = mysql.format(query);

  connection.query(options, function (err, result, fields) {
    if (err) throw err;
    res.json(result);
  });

});

// Find duplicate by username [เฉพาะ admin]
router.get('/duplicate/admin/:username', function(req, res, next) {

  let query = "SELECT * FROM `user_login` ul " +
              "WHERE ul.username = ? ";
  let options = mysql.format(query, [req.params.username]);

  connection.query(options, function (err, result, fields) {
    if (err) throw err;
    res.json(result);
  });
});

// Change password
router.put('/change/:username/:password', function(req, res, next) {
  let decodePassword = sha1(req.params.password);

  let query = "UPDATE user_login SET " +
            "password = ? " +
            "WHERE username = ? ";
  let options = mysql.format(query, [decodePassword, req.params.username]);

  connection.query(options, function (err, result, fields) {
    if (err) throw err;
    res.json(result);
  });
});

// delete user
router.delete('/:userLogin/:userId', function(req, res, next) {

  let query = "DELETE FROM user_login " +
              "WHERE user_login_id = ? ";
  let options = mysql.format(query, [req.params.userLogin]);

  let query2 = "DELETE FROM `user` " +
              "WHERE user_id = ? ";
  let options2 = mysql.format(query2, [req.params.userId]);

  connection.query(options2, function (err, result, fields) {
    if (err) throw err;
  });

  connection.query(options, function (err, result, fields) {
    if (err) throw err;
    res.json(result);
  });
});

// Login [ใช้ร่วมกัน]
router.get('/login/:username/:password', function(req, res, next) {
  let decodePassword = sha1(req.params.password);

  new Promise((resolve, reject) => {
    let query = "SELECT * FROM `user_login` ul " +
                "WHERE ul.username = ? " +
                "AND ul.password = ? ";
    let options = mysql.format(query, [req.params.username, decodePassword]);

    connection.query(options, function(err, result) {
      if (err) throw err;
      resolve(result);
    });
  })
  .then((result) => {
    if(result.length == 0){
      res.json(result);
    } else {
      if(result[0].admin_flag === 'N'){
        let query = "SELECT * FROM `user_login` ul " + 
                    "INNER JOIN `user` u ON u.user_id = ul.user_id " +
                    "WHERE ul.username = ? " +
                    "AND ul.password = ? ";
        let options = mysql.format(query, [req.params.username, decodePassword]);

        connection.query(options, function (err, result, fields) {
          if (err) throw err;
          res.json(result);
        });
      } else {
        let query = "SELECT * FROM `user_login` ul " + 
                    "INNER JOIN `admin` a ON a.admin_id = ul.user_id " +
                    "WHERE ul.username = ? " +
                    "AND ul.password = ? ";
        let options = mysql.format(query, [req.params.username, decodePassword]);

        connection.query(options, function (err, result, fields) {
          if (err) throw err;
          res.json(result);
        });
      }
    }
  })
  .catch((err) => {
    console.log(err);
  });
});

// Find by username [ใช้ร่วมกัน]
router.get('/info/:userId/:flag', function(req, res, next) {
  let resultObj;
  let query;

  let promise1 = new Promise((resolve, reject) => {
    if(req.params.flag === 'N'){
      query = "SELECT * FROM `user_login` ul " +
              "INNER JOIN `user` u ON u.user_id = ul.user_id " +
              "WHERE ul.user_id = ? ";
    } else if (req.params.flag === 'Y') {
      query = "SELECT * FROM `user_login` ul " +
              "INNER JOIN `admin` a ON a.admin_id = ul.user_id " +
              "WHERE ul.user_id = ? ";
    }

    let options = mysql.format(query, [req.params.userId]);

    connection.query(options, function (err, result, fields) {
      if (err) throw err;
      resultObj = JSON.parse(JSON.stringify(result));
      resolve();
    });
  });

  let promise2 = new Promise((resolve, reject) => {
    let query = "SELECT * FROM `disease` d " +
                "WHERE d.user_id = ? ";
    let options = mysql.format(query, [req.params.userId]);

    connection.query(options, function (err, result, fields) {
      if (err) throw err;
      resultObj.disease = JSON.parse(JSON.stringify(result));
      resolve();
    });
  });

  let promise3 = new Promise((resolve, reject) => {
    let query = "SELECT * FROM `medicine` m " +
                "WHERE m.user_id = ? ";
    let options = mysql.format(query, [req.params.userId]);

    connection.query(options, function (err, result, fields) {
      if (err) throw err;
      resultObj.medicine = JSON.parse(JSON.stringify(result));
      resolve();
    });
  });

  Promise.all([promise1, promise2, promise3]).then((result) => {
    // console.log(resultObj);
    res.json({ 
      userInfo: resultObj,
      disease: resultObj.disease, 
      medicine: resultObj.medicine
    });
  }).catch((err) => {
    console.log(err);
  });

});

// Find by username and identification_id [เฉพาะ user]
router.get('/:username/:identificationId', function(req, res, next) {
  let query = "SELECT * FROM `user_login` ul " +
              "INNER JOIN `user` u ON u.user_id = ul.user_id " +
              "WHERE ul.username = ? " +
              "OR u.identification_id = ? ";
  let options = mysql.format(query, [req.params.username, req.params.identificationId]);

  connection.query(options, function (err, result, fields) {
    if (err) throw err;
    res.json(result);
  });
});

// Find duplicate by identification_id without username [เฉพาะ user]
router.get('/duplicate/:username/:identificationId', function(req, res, next) {
  let query = "SELECT * FROM `user_login` ul " +
              "INNER JOIN `user` u ON u.user_id = ul.user_id " +
              "WHERE ul.username <> ? " +
              "AND u.identification_id = ? ";
  let options = mysql.format(query, [req.params.username, req.params.identificationId]);

  connection.query(options, function (err, result, fields) {
    if (err) throw err;
    res.json(result);
  });
});

// Insert user [เฉพาะ user]
router.post('/', function(req, res, next) {
  let userObj = req.body;
  let encodePassword = sha1(userObj.password);
  let insertId;

  new Promise((resolve, reject) => {
    let query = "INSERT INTO `user` (`title_name`, `firstname`, `lastname`, `identification_id`, `process`, `create_user`) VALUES (?, ?, ?, ?, ?, ?) ";
    let options = mysql.format(query, [userObj.titleName, 
                                      userObj.firstname, 
                                      userObj.lastname,
                                      userObj.identificationId,
                                      userObj.process,
                                      userObj.createUser]);
    
    connection.query(options, function(err, result) {
      if (err) throw err;
      insertId = result.insertId;
      resolve(result);
    });
  })
  .then((result) => {
    let queryUserLogin = "INSERT INTO `user_login` (`user_id`, `username`, `password`, `process`, `create_user`, admin_flag) VALUES (?, ?, ?, ?, ?, 'N')";
    let optionsUserLogin = mysql.format(queryUserLogin, [insertId, 
                                                userObj.username, 
                                                encodePassword,
                                                userObj.process, 
                                                userObj.createUser, 
                                                userObj.updateUser]);

    connection.query(optionsUserLogin, function(err, result) {
      if (err) throw err;
      res.json(result);
    });
  })
  .catch((err) => {
    console.log(err);
  });

});

// Update user by user_id [เฉพาะ user]
router.put('/', function(req, res, next) {
  let userObj = req.body;
  let birthDate = new Date(userObj.birthdate);

  // console.log(userObj);

  let promise1 = new Promise((resolve, reject) => {
    // Update user info
    // console.log("Update user info"); 

    let query = "UPDATE `user` SET " +
            "title_name = ? ," +
            "firstname = ? ," +
            "lastname = ? ," +
            "birthdate = ? ," +
            "sex = ? ," +
            "identification_id = ? ," +
            "weight = ? ," +
            "height = ? ," +
            "waistline = ? ," +
            "bmi = ? ," +
            "scr = ? ," +
            "kidney_stage = ? ," +
            "process = ? ," +
            "update_user = ? ," +
            "update_datetime = NOW() " +
            "WHERE user_id = ? ";
  let options = mysql.format(query, [userObj.title_name, 
                                    userObj.firstname, 
                                    userObj.lastname, 
                                    birthDate, 
                                    userObj.sex, 
                                    userObj.identification_id, 
                                    userObj.weight,
                                    userObj.height,
                                    userObj.waistline,
                                    userObj.bmi,
                                    userObj.scr,
                                    userObj.kidney_stage,
                                    userObj.process,
                                    userObj.username,
                                    userObj.user_id]);

    connection.query(options, function(err, result) {
      if (err) throw err;
      resolve(result);
    });
  });
  
  let promise2 = new Promise((resolve, reject) => {
    // Delete old disease
    // console.log("Delete old disease"); 

    let query = "DELETE FROM disease WHERE user_id = ?";
    let options = mysql.format(query, [userObj.user_id]);

    connection.query(options, function(err, result) {
      if (err) throw err;
      resolve(result);
    });
  });
  
  let promise3 = new Promise((resolve, reject) => {
    // Add new disease
    // console.log("Add new disease"); 

    let diseaseList = userObj.disease;
    let query = "INSERT INTO `disease` (`user_id`, `disease_name`, `process`, `create_user`, `update_user`) VALUES ?";
    let value = [];

    if(diseaseList != null && diseaseList.length > 0){
      for (let item1 of diseaseList) {
        let temp = [userObj.user_id, item1.disease_name, userObj.process, userObj.create_user, userObj.update_user];
        value.push(temp);
      }

      connection.query(query, [value], function(err, result) {
        if (err) throw err;
        resolve(result);
      });
    } else {
      resolve();
    }
  });
  
  let promise4 = new Promise((resolve, reject) => {
    // Delete old medicine
    // console.log("Delete old medicine"); 

    let query = "DELETE FROM medicine WHERE user_id = ?";
    let options = mysql.format(query, [userObj.user_id]);

    connection.query(options, function(err, result) {
      if (err) throw err;
      resolve(result);
    });
  });
  
  let promise5 = new Promise((resolve, reject) => {
    // Add new medicine
    // console.log("Add new medicine"); 

    let medicineList = userObj.medicine;
    let query = "INSERT INTO `medicine` (`user_id`, `medicine_name_th`, `medicine_name_en`, `medicine_taking`, `process`, `create_user`, `update_user`) VALUES ?";
    let value = [];

    if(medicineList != null && medicineList.length > 0){
      for (let item of medicineList) {
        let temp = [userObj.user_id, item.medicine_name_th, item.medicine_name_en, item.medicine_taking, userObj.process, userObj.create_user, userObj.update_user];
        value.push(temp);
      }

      connection.query(query, [value], function(err, result) {
        if (err) throw err;
        resolve(result);
      });
    } else {
      resolve();
    }
  });

  let promise6 = new Promise((resolve, reject) => {
    // Update user login

    let query = "UPDATE `user_login` SET " +
            "image_path = ? ," +
            "process = ? ," +
            "update_user = ? ," +
            "update_datetime = NOW() " +
            "WHERE user_id = ? ";
    let options = mysql.format(query, [userObj.image_path,
                                      userObj.process,
                                      userObj.username,
                                      userObj.user_id]);

    connection.query(options, function(err, result) {
      if (err) throw err;
      resolve(result);
    });
  });

  Promise.all([promise1, promise2, promise3, promise4, promise5, promise6]).then((result) => {
    res.json(result);
  }).catch((err) => {
    console.log(err);
  });

});

// Update user by user_id [เฉพาะ user]
router.put('/admin/', function(req, res, next) {
  let userObj = req.body;

  let promise1 = new Promise((resolve, reject) => {
    let query = "UPDATE `admin` SET " +
              "title_name = ? ," +
              "firstname = ? ," +
              "lastname = ? ," +
              "mobile = ? ," +
              "address = ? ," +
              "email = ? ," +
              "process = ? ," +
              "update_user = ? ," +
              "update_datetime = NOW() " +
              "WHERE admin_id = ? ";
    let options = mysql.format(query, [userObj.title_name, 
                                      userObj.firstname, 
                                      userObj.lastname, 
                                      userObj.mobile, 
                                      userObj.address, 
                                      userObj.email,
                                      userObj.process,
                                      userObj.username,
                                      userObj.admin_id]);

    connection.query(options, function(err, result) {
      if (err) throw err;
      resolve(result);
    });
  });

  let promise2 = new Promise((resolve, reject) => {
    let query = "UPDATE `user_login` SET " +
            "image_path = ? ," +
            "process = ? ," +
            "update_user = ? ," +
            "update_datetime = NOW() " +
            "WHERE user_id = ? ";
    let options = mysql.format(query, [userObj.image_path,
                                      userObj.process,
                                      userObj.username,
                                      userObj.user_id]);

    connection.query(options, function(err, result) {
      if (err) throw err;
      resolve(result);
    });
  });

  Promise.all([promise1, promise2]).then((result) => {
    res.json(result);
  }).catch((err) => {
    console.log(err);
  });
});

// Insert user [เฉพาะ admin]
router.post('/admin/', function(req, res, next) {
  let userObj = req.body;
  let encodePassword = sha1(userObj.password);
  let insertId;

  new Promise((resolve, reject) => {
    let query = "INSERT INTO `admin` (`title_name`, `firstname`, `lastname`, `mobile`, `address`, `email`, `process`, `create_user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";
    let options = mysql.format(query, [userObj.title_name, 
                                      userObj.firstname, 
                                      userObj.lastname,
                                      userObj.mobile,
                                      userObj.address,
                                      userObj.email,
                                      userObj.process,
                                      userObj.create_user]);
    
    connection.query(options, function(err, result) {
      if (err) throw err;
      insertId = result.insertId;
      resolve(result);
    });
  })
  .then((result) => {
    let queryUserLogin = "INSERT INTO `user_login` (`user_id`, `username`, `password`, `process`, `create_user`, admin_flag) VALUES (?, ?, ?, ?, ?, 'Y')";
    let optionsUserLogin = mysql.format(queryUserLogin, [insertId, 
                                                userObj.username, 
                                                encodePassword,
                                                userObj.process, 
                                                userObj.createUser, 
                                                userObj.updateUser]);

    connection.query(optionsUserLogin, function(err, result) {
      if (err) throw err;
      res.json(result);
    });
  })
  .catch((err) => {
    console.log(err);
  });

});

module.exports = router;
