var express = require('express');
var router = express.Router();
var connection = require('../conn');
const mysql = require('mysql');

function setKey(result) {
  for (let i = 0; i < result.length; i++) {
    result[i].key = (i + 1);
  }
  return result;
}
router.get('/', function (req, res, next) {
  let query = "SELECT * FROM `food_type` f ";
  let options = mysql.format(query, [req.params.username]);

  connection.query(options, function (err, result, fields) {
    result = setKey(result);
    if (err) throw err;
    res.json(result);
  });
});

router.get('/:foodTypeID', function (req, res, next) {
  let query = "SELECT * FROM `food_type` f  WHERE f.food_type_id = ?";
  let options = mysql.format(query, [req.params.foodTypeID]);

  connection.query(options, function (err, result, fields) {
    if (err) throw err;
    res.json(result);
  });
});

router.post('/delete/:itemId', function (req, res, next) {
  console.log(req.params);
  let query = "DELETE  FROM `food_type`   WHERE food_type_id = ?";
  let options = mysql.format(query, [req.params.itemId]);

  connection.query(options, function (err, result, fields) {
    if (err) throw err;
    res.json(result);
  });
});


//Insert Food_type
router.post('/', function (req, res, next) {

  let foodObj = req.body;
  console.log(foodObj);

  let query = "INSERT INTO `food_type` (`food_type_name_th`, `food_type_name_en`, `process`, `create_user`, `create_datetime`,`update_datetime`,`food_type_image_url`) VALUES (?, ?, ?, ?, ?,?,?) ";
  let options = mysql.format(query, [
    foodObj.foodTypeNameTh,
    foodObj.foodTypeNameEn,
    foodObj.process,
    foodObj.createUser,
    new Date(),
    null,
    foodObj.image_path]);

  connection.query(options, function (err, result) {
    if (err) throw err;
    res.json(result);
  });
});

router.put('/', function (req, res, next) {
  let userObj = req.body;
  console.log(userObj);

  let query = "UPDATE `food_type` SET " +
    "food_type_name_th = ? ," +
    "food_type_name_en = ? ," +
    "process = ? ," +
    "update_user = ? ," +
    "update_datetime = ? ," +
    "food_type_image_url = ? " +
    "WHERE food_type_id = ? ";
  let options = mysql.format(query, [userObj.food_type_name_th,
  userObj.food_type_name_en,
  userObj.process,
  userObj.updateUser,
  new Date(),
  userObj.image_path,
  userObj.food_type_id]);

  connection.query(options, function (err, result) {
    if (err) throw err;
    res.json(result);
  });
});


module.exports = router;