var express = require('express');
var router = express.Router();
var connection = require('../conn');
const mysql = require('mysql');

// Find by foodType
router.get('/foodType/:foodTypeId', function (req, res, next) {
  let query = "SELECT * FROM `food` f " +
    "WHERE f.food_type_id = ? ";
  let options = mysql.format(query, [req.params.foodTypeId]);

  connection.query(options, function (err, result, fields) {
    if (err) throw err;
    res.json(result);
  });
});

// Find by foodId
router.get('/:foodId', function (req, res, next) {
  let query = "SELECT * FROM `food` f " +
    "WHERE f.food_id = ? ";
  let options = mysql.format(query, [req.params.foodId]);

  connection.query(options, function (err, result, fields) {
    if (err) throw err;
    res.json(result);
  });
});

// Insert foodDetails
router.post('/', function (req, res, next) {
  let foodObj = req.body;

  let query = "INSERT INTO `food` (`food_type_id`, `food_name_th`, `food_name_en`, `sodium`,`unit`, `process`, `create_user`, `update_user`, `image_path`) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?) ";
  let options = mysql.format(query, [foodObj.foodTypeId,
    foodObj.foodNameTh,
    foodObj.foodNameEn,
    foodObj.sodium,
    foodObj.unit,
    foodObj.process,
    foodObj.createUser,
    foodObj.updateUser,
    foodObj.image_path,
  ]);

  connection.query(options, function (err, result) {
    if (err) throw err;
    res.json(result);
  });
});

// Update foodDetails
router.put('/', function (req, res, next) {
  let foodObj = req.body;


  let query = "UPDATE `food` SET " +
    "food_name_th = ? ," +
    "food_name_en = ? ," +
    "sodium=? ," +
    "unit = ? ," +
    "food_type_id = ? ," +
    "process = ? ," +
    "update_user = ? ," +
    "image_path = ? " +
    "WHERE food_id = ? ";
  let options = mysql.format(query, [
    foodObj.food_name_th,
    foodObj.food_name_en,
    foodObj.sodium,
    foodObj.unit,
    foodObj.food_type_id,
    foodObj.process,
    foodObj.updateUser,
    foodObj.image_path,
    foodObj.food_id
  ]);
  console.log(options)

  connection.query(options, function (err, result) {
    if (err) throw err;
    res.json(result);
  });
});

// delete foodDetails
router.post('/delete/:itemId', function(req, res, next) {
  console.log(req.params);
let query = "DELETE  FROM `food`   WHERE food_id = ?";
let options = mysql.format(query, [req.params.itemId]);

connection.query(options, function (err, result, fields) {
  if (err) throw err;
  res.json(result);
});
});




module.exports = router;