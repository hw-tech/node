var express = require('express');
var router = express.Router();
var connection = require('../conn');
var sha1 = require('sha1');
var dateFormat = require('dateformat');

const mysql = require('mysql');

router.get('/today/:userId/:date', function(req, res, next) {

  let query = "SELECT * FROM `user_eat` e " +
              "INNER JOIN `food` f ON f.food_id = e.food_id " +
              "WHERE CAST(e.eat_date AS DATE) = ? " + 
              "AND e.user_id = ? ";
  let options = mysql.format(query, [req.params.date, req.params.userId]);

  connection.query(options, function (err, result, fields) {
    if (err) throw err;
    res.json(result);
  });
  
});

router.get('/graph/:userId/:formDate/:toDate', function(req, res, next) {

  let query = "SELECT SUM(e.sodium * e.eat_amount) as SumAmount, DATE(e.eat_date) DateOnly " +
              "FROM user_eat e " +
              "WHERE e.user_id = ? " + 
              "AND e.eat_date BETWEEN DATE( ? ) AND DATE( ? ) " + 
              "GROUP BY DATE(e.eat_date) ";
  let options = mysql.format(query, [req.params.userId, req.params.formDate, req.params.toDate]);

  connection.query(options, function (err, result, fields) {
    if (err) throw err;
    res.json(result);
  });
  
});

router.post('/', function(req, res, next) {
    let consumptionObj = req.body;

    let dt = new Date(consumptionObj.eat_date);

    let query = "INSERT INTO `user_eat` (`user_id`, `food_id`, `eat_date`, `eat_amount`, `sodium`, `food_name`, `process`, `create_user`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    let options = mysql.format(query, [consumptionObj.user_id, 
                                        consumptionObj.food_id, 
                                        dt, 
                                        consumptionObj.eat_amount,
                                        consumptionObj.sodium,
                                        consumptionObj.food_name,
                                        consumptionObj.process, 
                                        consumptionObj.create_user]);

    connection.query(options, function(err, result) {
    if (err) throw err;
    res.json(result);
    });
  
});

router.post('/old', function(req, res, next) {
  let consumptionObj = req.body;

  let eatDate = dateFormat(new Date(consumptionObj.eat_date), "yyyy-mm-dd h:MM:ss");
  console.log(eatDate);

  let query = "INSERT INTO `user_eat` (`user_id`, `food_id`, `eat_date`, `eat_amount`, `sodium`, `food_name`, `process`, `create_user`) VALUES (?, ?, CAST(? AS DATETIME), ?, ?, ?, ?, ?)";
  let options = mysql.format(query, [consumptionObj.user_id, 
                                      consumptionObj.food_id, 
                                      eatDate, 
                                      consumptionObj.eat_amount,
                                      consumptionObj.sodium,
                                      consumptionObj.food_name,
                                      consumptionObj.process, 
                                      consumptionObj.create_user]);

  connection.query(options, function(err, result) {
  if (err) throw err;
  res.json(result);
  });

});

router.delete('/:eatId', function(req, res, next) {

  let query = "DELETE FROM `user_eat` WHERE `user_eat_id` = ? ";
  let options = mysql.format(query, [req.params.eatId]);

  connection.query(options, function(err, result) {
  if (err) throw err;
  res.json(result);
  });

});

module.exports = router;
