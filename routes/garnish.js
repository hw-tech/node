var express = require('express');
var router = express.Router();
var connection = require('../conn');
const mysql = require('mysql');

function setKey(result) {
    for (let i = 0; i < result.length; i++) {
        result[i].key = (i + 1);
    }
    return result;
}
router.get('/', function (req, res, next) {
    let query = "SELECT * FROM `seasoning` f ";
    let options = mysql.format(query, [req.params.username]);

    connection.query(options, function (err, result, fields) {
        result = setKey(result)
        if (err) throw err;
        res.json(result);
    });
});

router.get('/:foodTypeID', function (req, res, next) {
    let query = "SELECT * FROM `seasoning` s  WHERE s.seasoning_id = ?";
    let options = mysql.format(query, [req.params.foodTypeID]);

    connection.query(options, function (err, result, fields) {
        if (err) throw err;
        res.json(result);
    });
});

router.post('/delete/:itemId', function (req, res, next) {
    console.log(req.params);
    let query = "DELETE  FROM `seasoning`   WHERE seasoning_id = ?";
    let options = mysql.format(query, [req.params.itemId]);

    connection.query(options, function (err, result, fields) {
        if (err) throw err;
        res.json(result);
    });
});


//Insert seasoning
router.post('/', function (req, res, next) {

    let garnish = req.body;

    let query = "INSERT INTO `seasoning` (`seasoning_name_th`, `seasoning_name_en`,`sodium`,`unit`, `process`, `create_user`, `create_datetime`,`update_datetime`,`seasoning_image_url`) VALUES (?, ?, ?, ?, ?,?,?,?,?) ";
    let options = mysql.format(query, [
        garnish.garnishNameTh,
        garnish.garnishNameEn,
        garnish.sodium,
        garnish.unit,
        garnish.process,
        garnish.createUser,
        new Date(),
        null,
        garnish.image_path,
    ]);

    connection.query(options, function (err, result) {
        if (err) throw err;
        res.json(result);
    });
});

router.put('/', function (req, res, next) {
    let userObj = req.body;
    console.log(userObj);


    let query = "UPDATE `seasoning` SET " +
        "seasoning_name_th = ? ," +
        "seasoning_name_en = ? ," +
        "sodium=? ," +
        "unit = ? ," +
        "process = ? ," +
        "update_user = ? ," +
        "update_datetime = ? , " +
        "seasoning_image_url = ? "+
        "WHERE seasoning_id = ? ";
    ;
    let options = mysql.format(query, [
        userObj.seasoning_name_th,
        userObj.seasoning_name_en,
        userObj.sodium,
        userObj.unit,
        userObj.process,
        userObj.updateUser,
        new Date(),
        userObj.seasoning_image_url,
        userObj.seasoning_id]);
    console.log(options);

    connection.query(options, function (err, result) {
        if (err) throw err;
        res.json(result);
    });
});

router.get('/:garnishID', function (req, res, next) {
    let query = "SELECT * FROM `seasoning` s " +
        "WHERE s.seasoning_id = ? ";
    let options = mysql.format(query, [req.params.garnishID]);

    connection.query(options, function (err, result, fields) {
        if (err) throw err;
        res.json(result);
    });
});

module.exports = router;